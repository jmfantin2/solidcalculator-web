import React, { useState } from "react";
import DatePicker from "react-datepicker";

import "react-datepicker/dist/react-datepicker.css";

export function DRETable() {
  const [startDate, setStartDate] = useState(new Date());
  const [endDate, setEndDate] = useState(new Date());

  return (
    <>
      <table className="tw_dre_table">
        <tr>
          <th>Empresa (CNPJ)</th>
          <th>Faturamento Total</th>
          <th>Custos Variáveis</th>
          <th>M. Contribuição Nominal Total</th>
          <th>Custos Fixos</th>
          <th>Lucro</th>
          <th>Ponto de Equilíbrio</th>
        </tr>
        <tr>
          <td>96.245.212/0001-08</td>
          <td>611.99</td>
          <td>270.44</td>
          <td>341.55</td>
          <td>200.0</td>
          <td>141.55</td>
          <td>358.36</td>
        </tr>
      </table>
      <div className="flex flex-col justify-between">
        <div className="ml-[0.5rem] mt-[6px] flex flex-row gap-2">
          <div className="max-h-[6rem]  text-center text-white font-bold bg-accent_greyed p-2 ">
            Data Inicial
            <DatePicker
              selected={startDate}
              onChange={(date: Date) => setStartDate(date)}
              showMonthYearPicker
              inline
            />
          </div>
          <div className="max-h-[6rem]  text-center text-white font-bold bg-accent_greyed p-2 ">
            Data Final
            <DatePicker
              selected={endDate}
              onChange={(date: Date) => setEndDate(date)}
              showMonthYearPicker
              inline
            />
          </div>
        </div>
        <div className="tw_dre_table_sub">
          <button>Consultar</button>
        </div>
      </div>
    </>
  );
}
