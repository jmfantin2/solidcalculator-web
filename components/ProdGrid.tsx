import { useState } from "react";

export function ProdGrid(props) {
  const [precoV, setPrecoV] = useState(2.07);
  const [quantV, setQuantV] = useState(1);
  return (
    <>
      <header className="tw_prod_header">
        <p>
          {props.p === "caneta" ? "Caneta BIC" : "Borracha Mercur"} •{" "}
          {props.m
            ? props.m.substring(4, 7) + "/" + props.m.substring(11, 16)
            : ""}
        </p>
      </header>

      <main className="tw_prod_ths">
        <div>ID</div>
        <div>
          Situação
          <br />
          Venda
        </div>
        <div>Despesas</div>
        <div>
          Margem
          <br />
          Nominal
        </div>
        <div>
          Margem
          <br />
          Percentual
        </div>
        <div>Custo</div>
        <div>
          Preço
          <br />
          Mínimo
        </div>
        <div>
          Preço
          <br />
          Venda
        </div>
        <div>
          Margem
          <br />
          Contribuição
        </div>
        <div>
          Quantidade
          <br />
          Vendida
        </div>
      </main>

      <main className="tw_prod_grid">
        <div>1</div>
        <div>Shopee</div>
        <div>13.00%</div>
        <div>R$ 0.00</div>
        <div>0%</div>
        <div>R$ 1.80</div>
        <div>R$ 2.07</div>
        <div>
          R$ <input type="text" placeholder={precoV.toString()} />
        </div>
        <div>R$ 0</div>
        <div>
          <input type="text" placeholder={quantV.toString()} />
        </div>
      </main>
    </>
  );
}
