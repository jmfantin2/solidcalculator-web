import { DRETable } from "../../components/DRETable";

export default function DREPage({}) {
  return (
    <main className="tw_container">
      <div className="flex justify-center">
        <DRETable />
      </div>
    </main>
  );
}
