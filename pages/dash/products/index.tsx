import { useState } from "react";
import shift from "classnames";
import DatePicker from "react-datepicker";
import { Dropdown } from "flowbite-react";

import "react-datepicker/dist/react-datepicker.css";
import Link from "next/link";

export default function ProductsPage({}) {
  const [month, setMonth] = useState(new Date());
  const [product, setProduct] = useState("caneta");

  return (
    <main className="tw_container mt-8 flex flex-row items-start h-auto justify-center gap-8">
      <div className="flex flex-col items-center justify-center gap-3">
        <div>1) Escolha um mês</div>
        <DatePicker
          selected={month}
          onChange={(date: Date) => setMonth(date)}
          showMonthYearPicker
          inline
        />
      </div>
      <div className="flex flex-col items-center justify-center gap-3">
        <div>2) Selecione um produto</div>
        <Dropdown label="seus produtos">
          <Link
            href={{
              pathname: "/dash/products/report",
              query: { prod: "caneta", mth: `${month}` },
            }}
          >
            <Dropdown.Item>Caneta BIC</Dropdown.Item>
          </Link>
          <Link
            href={{
              pathname: "/dash/products/report",
              query: { prod: "borracha", mth: `${month}` },
            }}
          >
            <Dropdown.Item>Borracha Mercur</Dropdown.Item>
          </Link>
        </Dropdown>
      </div>
    </main>
  );
}

/**
 * 
      lista os caras
      deixa selecionar um mês no datepicker
      botão Link href /report com query passando produto e mes escolhido
 */
