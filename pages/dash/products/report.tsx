import { useRouter } from "next/router";
import { ProdGrid } from "../../../components/ProdGrid";

export default function ReportPage({ product }) {
  const router = useRouter();
  const { prod, mth } = router.query;

  return (
    <main className="tw_container">
      <ProdGrid p={prod} m={mth} />
      <div className="tw_prod_table_sub">
        <button>Salvar</button>
        <button>Simular</button>
      </div>
    </main>
  );
}
